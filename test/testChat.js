var SocketIoClient = require('./socketIoClient');
var crypto = require('crypto');
var colors = require('colors');

module.exports = function (testComplete){

    var config = require('../config.json');
    var server = {
        config: config
    }
    console.log(server);
    var database = new (require('../server_modules/database'))(server);
    console.log(database);

    var usersToCreate = 100;
    var users = [];

    startTests();

    function startTests () {
        beforeTests();
    }

    function beforeTests(done){

        database.query('delete from messages;', function(error, response){

            if (!error){
                console.log('messages deleted.');

                database.query('delete from users;', function(error, response){


                    if (!error){
                        console.log('cleared user table.');

                        var userId = 0;
                        createUser(userId);

                        function createUser(userId) {

                            var newUser = {
                                username: 'user'+userId,
                                password: 'user'+userId,
                                email: 'user'+userId+'@glitr.io',
                                loginHash: 'dummyHash',
                            }

                            database.create('users', newUser, function(error, response){

                                if (!error){
                                    userId++;
                                    if (userId === usersToCreate){
                                        console.log('created new users');
                                        loggingInUsers();
                                    } else {
                                        createUser(userId);
                                    }
                                } else {
                                    throw error;
                                }

                            });
                        }
                    } else {
                        throw error
                    }

                });
            }
        });
    }

    function loggingInUsers(){

        var userId = 0;
        logInSingleUser(userId);

        function logInSingleUser(userId){
            users.push(new SocketIoClient());

            users[userId].connectToServer('user'+userId, 'user'+userId, null, function(error, response){

                if (!error){
                    console.log('logged in single users.');
                    logOutSingleUser(userId);
                } else {
                    console.log('failed to log in single user. trying again.');
                    logInSingleUser(userId);
                }
            });
        }

        function logOutSingleUser(userId){
            users[userId].disconectFromServer(function(){
                console.log('logged out single user');
                users = [];
                users[userId] = new SocketIoClient();
                debugger;
                logInPredefinedNumberOfUsers(userId);
            });
        }

        function logInPredefinedNumberOfUsers(userId){

            users[userId].connectToServer('user'+userId, 'user'+userId, null, function(error, response){
                if (!error){
                    console.log('successfully logged in user: user'+userId);
                    if (userId === usersToCreate-1){
                        waitForUsersToConnect();
                    } else {
                        userId++;
                        users[userId] = new SocketIoClient();
                        logInPredefinedNumberOfUsers(userId);
                    }
                } else {
                    console.log('failed to log in user: user'+userId+'. trying again.');
                    logInPredefinedNumberOfUsers(userId);
                }
            });

        }
    }

    function waitForUsersToConnect(){
        var connectedUsers = 0;

        for(var x = 0; x < usersToCreate; x++){
            if (users[x].isConnected()){
                connectedUsers++;
            }
        }

        console.log('connectedUsers =', connectedUsers);

        if (connectedUsers < usersToCreate){
            setTimeout(waitForUsersToConnect, 1000);
        } else {
            console.log('logged in predefined number of users');
            // sendMessagedToRandomOtherUsers();
            sendMessageFromOneUserToAnother();
        }
    }

    function sendMessageFromOneUserToAnother(){

        var message = {
            to: 'user1',
            from: 'user0',
            message: 'message0',
            identifier: crypto.randomBytes(20).toString('hex')
        }

        users[1].expectedMessages.push(message);

        users[0].emitMessageToUser(users[0].loginHash, message, function(error, response){
            if (error){
                // throw error;
                console.log(error.red);
            } else {
                console.log(response);
                sendMessagedToRandomOtherUsers();
            }
        });
    }



    function sendMessagedToRandomOtherUsers(){
        var messagesToSend = 100;

        for (var x = 0; x < messagesToSend; x++){

            // (function(usersToCreate, crypto, users){

                var randomSender = Math.floor(Math.random() * usersToCreate);
                var randomRecipient = Math.floor(Math.random() * usersToCreate);
                var messageToSend = 'message'+x;

                while (randomSender === randomRecipient){
                    randomRecipient = Math.floor(Math.random() * usersToCreate);
                }

                var message = {
                    to: 'user'+randomRecipient,
                    from: 'user'+randomSender,
                    message: messageToSend,
                    identifier: crypto.randomBytes(20).toString('hex')
                };

                users[randomRecipient].expectedMessages.push(message);

                users[randomSender].emitMessageToUser(users[randomSender].loginHash, message, function(error, response){
                    if (error){
                        // throw error;
                        console.log(error.red);
                    } else {
                        console.log('sent message', x, 'of', messagesToSend);
                        console.log(response);
                    }
                });



            // }(usersToCreate, crypto, users));

        }

        waitForUsersToRecieveMessages();

        function waitForUsersToRecieveMessages(){

            var remainingMessages = 0;
            var messagesPending = [];

            for (var x = 0; x < users.length; x++){
                remainingMessages += users[x].expectedMessages.length;

                if (users[x].expectedMessages.length > 0){
                    messagesPending.push(users[x].expectedMessages);
                }
            }

            if (remainingMessages > 0){
                setTimeout(function(){
                    console.log('remaining messages to be sent:'.red, remainingMessages);
                    console.log('----------------------------------------------');
                    console.log('messages pending:', messagesPending);
                    console.log('----------------------------------------------');
                    waitForUsersToRecieveMessages();
                },1000);
            } else {
                console.log('all messages sent.');
                clearDatabase();
            }
        }
    }



    function clearDatabase(){
        database.query('delete from messages;', function(error, response){

            if (!error){
                console.log('messages deleted.');

                database.query('delete from users;', function(error, response){


                    if (!error){
                        console.log('cleared user table.');
                        testComplete();

                    } else {
                        throw error
                    }

                });
            }
        });
    }
}



    // function logOutUsers(done){
    //     users[0].disconectFromServer(function(){
    //         done();
    //     })
    // }



//             it('send messages to random other users', function(done){
//
//
//                 // var messages = [];
//
//                 // for (var x = 0; x < messagesToSend; x++){
//                 //
//                 //     var randomSender = Math.floor(Math.random() * usersToCreate);
//                 //     var randomRecipient = Math.floor(Math.random() * usersToCreate);
//                 //     var messageToSend = 'message'+x;
//                 //
//                 //     while (randomSender === randomRecipient){
//                 //         randomRecipient = Math.floor(Math.random() * usersToCreate);
//                 //     }
//                 //
//                 //     messages[x] = {
//                 //         to: 'user'+randomRecipient,
//                 //         from: 'user'+randomSender,
//                 //         message: messageToSend,
//                 //         identifier: crypto.randomBytes(20).toString('hex')
//                 //     };
//                 //
//                 //     users[randomRecipient].expectedMessages.push(messages[x]);
//                 //
//                 //     users[randomSender].emitMessageToUser(users[randomSender].loginHash, messages[x], function(error, response){
//                 //         if (error){
//                 //             throw error;
//                 //         }
//                 //     });
//                 //
//                 //     console.log('sent message', x, 'of', messagesToSend);
//                 //     console.log(messages[x]);
//                 // }
//
//
//
// /*
//                 var messagesToSend = 4;
//                 var messageIndex = 0;
//                 sendMessageToRandomOtherUser(messageIndex);
//
//                 function sendMessageToRandomOtherUser(messageIndex){
//
//                     console.log('creating message:', messageIndex);
//
//                     var randomSender = Math.floor(Math.random() * usersToCreate);
//                     var randomRecipient = Math.floor(Math.random() * usersToCreate);
//                     var messageToSend = 'message'+messageIndex;
//
//                     while (randomSender === randomRecipient){
//                         randomRecipient = Math.floor(Math.random() * usersToCreate);
//                     }
//
//                     var message = {
//                         to: 'user'+randomRecipient,
//                         from: 'user'+randomSender,
//                         message: messageToSend,
//                         identifier: crypto.randomBytes(20).toString('hex')
//                     };
//
//                     users[randomRecipient].expectedMessages.push(message);
//
//                     users[randomSender].emitMessageToUser(users[randomSender].loginHash, message, function(error, response){
//                         if (error){
//                             throw error;
//                         } else {
//
//                             if (messageIndex < messagesToSend){
//                                 sendMessageToRandomOtherUser(messageIndex+1);
//                             } else {
//                                 waitForUsersToRecieveMessaged();
//                             }
//                         }
//                     });
//                 }
// */
//
//                 var messagesToSend = 10;
//                 var messages = [];
//                 // generateRandomMessages(messageIndex);
//
//                 // function generateRandomMessages(index){
//                 for (var x = 0; x < messagesToSend; x++){
//                     console.log('creating message:', x, 'of', messagesToSend);
//
//                     var randomSender = Math.floor(Math.random() * usersToCreate);
//                     var randomRecipient = Math.floor(Math.random() * usersToCreate);
//                     var messageToSend = 'message'+x;
//
//                     while (randomSender === randomRecipient){
//                         randomRecipient = Math.floor(Math.random() * usersToCreate);
//                     }
//
//                     var message = {
//                         to: 'user'+randomRecipient,
//                         from: 'user'+randomSender,
//                         message: messageToSend,
//                         identifier: crypto.randomBytes(20).toString('hex')
//                     };
//
//                     users[randomRecipient].expectedMessages.push(message);
//
//                     messages.push({
//                         to: randomRecipient,
//                         from: randomSender,
//                         message: message
//                     });
//
//                     // users[randomSender].emitMessageToUser(users[randomSender].loginHash, message, function(error, response){
//                     //     if (!error){
//                     //         console.log('response:', index, 'of', messagesToSend);
//                     //         if (index < messagesToSend){
//                     //             messageIndex++;
//                     //             sendMessageToRandomOtherUser(messageIndex);
//                     //         } else if (index === messagesToSend){
//                     //             console.log('all messages sent -', 'response:', index, 'of', messagesToSend);
//                     //             waitForUsersToRecieveMessaged();
//                     //         } else {
//                     //             throw 'error!!!';
//                     //         }
//                     //     }
//                     // });
//
//                 }
//
//                 var messageIndex = 0;
//                 sendMessageToRandomOtherUser(messageIndex);
//                 var counter = 0;
//                 console.log('messages to send:', messages.length);
//
//                 function sendMessageToRandomOtherUser(x){
//                     console.log('sending message: ', x);
//                     // for (var x = 0; x < messages.length; x++){
//                         users[messages[x].from].emitMessageToUser(users[messages[x].from].loginHash, messages[x].message, function(error, response){
//                             if (!error){
//                                 if (x < messagesToSend-1){
//                                     x++;
//                                     setTimeout(
//                                         function(){
//                                             sendMessageToRandomOtherUser(x);
//                                         },
//                                         500
//                                     )
//
//                                 } else if (x === messagesToSend){
//                                     waitForUsersToRecieveMessaged();
//                                 } else {
//                                     throw 'error!!!';
//                                 }
//                             }
//                         });
//                     // }
//                 }
//
//
//                 // var messageCount1 = 0;
//                 //
//                 // for (var x = 0; x < usersToCreate; x++){
//                 //     messageCount1 += users[x].expectedMessages.length;
//                 // }
//                 // console.log('users online:', usersToCreate);
//                 // console.log('initial remaining messages:', messageCount1);
//
//                 function waitForUsersToRecieveMessaged(){
//                     var messageCount = 0;
//
//                     for (var x = 0; x < usersToCreate; x++){
//                         messageCount += users[x].expectedMessages.length;
//                     }
//
//                     console.log('remaining messages:', messageCount);
//
//                     if (messageCount > 0){
//                         setTimeout(waitForUsersToRecieveMessaged, 1000);
//                     } else {
//                         done();
//                     }
//                 }
//
//             });
//
//             // it('log out half of the pre-defined number of users', function(done){
//             //
//             //     for (var x = 0; x < usersToCreate; x++){
//             //         if (Math.floor(Math.random()*2) === 0){
//             //             users[x].disconnect();
//             //         }
//             //     }
//             //
//             //     var messagesToSend = 50;
//             //     var messages = [];
//             //
//             //     for (var x = 0; x < messagesToSend; x++){
//             //
//             //         var randomSender = Math.floor(Math.random() * usersToCreate);
//             //         var randomRecipient = Math.floor(Math.random() * usersToCreate);
//             //         var messagesToSend = 'message'+x;
//             //
//             //         while (randomSender === randomRecipient){
//             //             randomRecipient = Math.floor(Math.random() * usersToCreate);
//             //         }
//             //
//             //         messages[x] = {
//             //             to: 'user'+randomRecipient,
//             //             from: 'user'+randomSender,
//             //             message: messagesToSend,
//             //             identifier: crypto.randomBytes(20).toString('hex')
//             //         };
//             //
//             //         users[randomRecipient].expectedMessages.push({
//             //             from: 'user'+randomSender,
//             //             message: messages[x]
//             //         })
//             //
//             //         users[randomSender].emitMessageToUser(users[randomSender].loginHash, messages[x], function(error, response){
//             //             if (error){
//             //                 throw error;
//             //             }
//             //         });
//             //     }
//             //
//             //     var pendingMessage = messagesToSend;
//             //
//             //     while (pendingMessage > 0){
//             //         var messageCount = 0;
//             //
//             //         for (var x = 0; x < usersToCreate; x++){
//             //             if (users[x].connected){
//             //                 messageCount += users[x].expectedMessages.length;
//             //             }
//             //         }
//             //
//             //         console.log('remaining messages for online users:', messageCount);
//             //         pendingMessage = messageCount;
//             //     }
//             //
//             //     done();
//             //
//             // });
//             //
//             // it('log in offline users and assert pending messages are sent', function(done){
//             //
//             //     for (var x = 0; x < usersToCreate; x++){
//             //         if (!users[x].connected){
//             //             users[x].connectToServer('user'+x, 'user'+x, null, function(error, response){
//             //                 if (error){
//             //                     throw error
//             //                 }
//             //             });
//             //         }
//             //     }
//             //
//             //     var pendingMessage = 1;
//             //
//             //     while (pendingMessage > 0){
//             //         var messageCount = 0;
//             //
//             //         for (var x = 0; x < usersToCreate; x++){
//             //             messageCount += users[x].expectedMessages.length;
//             //         }
//             //
//             //         console.log('remaining messages:', messageCount);
//             //         pendingMessage = messageCount;
//             //     }
//             //
//             //     done();
//             //
//             // });
//
//             // it('users create groups', function(done){
//             //
//             // });
//             //
//             // it('add users to groups', function(done){
//             //
//             // });
//             //
//             // it('send messages to groups and assert the the message is recieved by all users', function(done){
//             //
//             // });
//             //
//             // it('remove group member and send message.', function(done){
//             //
//             // });
//             //
//             // it('non-admin is unable to create new admin', function(done){
//             //
//             // });
//             //
//             // it('admin can create new user', function(done){
//             //
//             // });
//             //
//             // it('new admin can add and remove new users', function(done){
//             //
//             // });
//
//         });
//
//         after(function(done){
//             database.query('delete from users;', function(error, response){
//
//                 if(!error){
//                     done();
//                     testComplete();
//                 } else {
//                     console.log(error);
//                 }
//             });
//         });
//     });
// }
