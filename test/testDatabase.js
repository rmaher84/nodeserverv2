module.exports = function(testComplete){

    var config = require('../config.json');
    var server = {
        config: config
    }
    console.log(server);
    var database = new (require('../server_modules/database'))(server);
    console.log(database);

    startTests();

    function startTests(){
        beforeAll();
    }

    function beforeAll () {
        database.query('delete from users;', deleteUsersResponse);

        function deleteUsersResponse(error, response){
            if (error){
                throw error;
            } else {
                testDatabaseConnection();
            }
        }
    }

    function testDatabaseConnection () {
        database.query('SELECT 1::int AS number', function(error, result){

            if (!error){
                if (result[0].number === 1){
                    testDatabaseQueries();
                } else {
                    throw 'expected: 1\nreturned:'+result[0].number;
                }
            } else {
                throw error;
            }

        });
    }

    function testDatabaseQueries () {

        var newUser = {
            username: 'clark',
            password: 'kent',
            email: 'clark@glitr.io',
            loginHash: 'dummyHash'
        }

        database.create('users', newUser, function(error, response){
            if (!error){
                getResultsFromUserTable();
            } else {
                throw error;
            }
        });

        function getResultsFromUserTable () {
            database.query('select * from users', function(error, result){
                if (!error){
                    if (!!result === true){
                        failsToGetResultFromTableThatDoesntExist();
                    };
                } else {
                    throw error;
                }
            });
        }

        function failsToGetResultFromTableThatDoesntExist () {
            database.query('select * from noSuchTable', function(error, result){
                if (error){
                    findClarkInUsersTable();
                }
            });
        }

        function findClarkInUsersTable () {
            database.query('select * from users where "username"=\'clark\'', function(error, result){
                if (!error){
                    if (result[0].username === 'clark'){
                        CRUDUsingSQLCommands();
                    }
                } else {
                    throw error;
                }
            });
        }

        function CRUDUsingSQLCommands () {
            database.query('insert into users (username, password, email, loginHash) values (\'john\', \'jones\', \'john@glitr.io\', \'dummyHash\');', function(error, result){
                if (!error){
                    database.query('select * from users where "username"=\'john\'', function(error, result){
                        if (!error){
                            if (result[0].username === 'john'){
                                    database.query('update users set email=\'martian.manhunter@test.com\' where username=\'john\'', function(error, result){
                                        if (!error){
                                            database.query('select * from users where "username"=\'john\'', function(error, result){
                                                if (!error){
                                                    if (result[0].email === 'martian.manhunter@test.com'){
                                                        database.query('delete from users where username=\'john\'', function(error, result){
                                                            if (!error){
                                                                if (result.length === 0){
                                                                    testDatabaseCrudInterface();
                                                                }
                                                            } else {
                                                                throw error;
                                                            }
                                                        });
                                                    }
                                                } else {
                                                    throw error;
                                                }
                                            });
                                        } else {
                                            throw error;
                                        }
                                    });
                            } else {
                                throw 'expected: \'john.jones\'\nreturned:'+result[0].username;
                            }
                        } else {
                            throw error;
                        }
                    });

                } else {
                    throw error;
                }
            });
        }

    }

    function testDatabaseCrudInterface (){

        createRecord();

        function createRecord(){
            database.create('users', {username: 'barry.allen', password:'barry.allen', email: 'barry.allen@test.com', loginHash: 'dummyHash'}, function(error, result){
                if (!error){
                    if(!!result){
                        readRecord();
                    }
                } else {
                    throw 'failed to create record';
                }
            });
        }

        function readRecord(){
            database.read('users', ['*'], {username: 'barry.allen'}, function(error, result){
                if (!error){
                    if (result[0].email === 'barry.allen@test.com'){
                        updateRecord();
                    }
                } else {
                    throw 'failed to create record';
                }
            });
        }

        function updateRecord(){

            database.update('users', {username: 'barry.allen'}, {email: 'the.flash@test.com'}, function(error, result){
                database.read('users', [], {username: 'barry.allen'}, function(error, result){
                    if (result[0].email === 'the.flash@test.com'){
                        deleteRecord();
                    }
                });
            });
        }

        function deleteRecord(){

            database.delete('users', {username: 'barry.allen'}, function(error, result){
                database.read('users', [], {username: 'barry.allen'}, function(error, result){
                    if (result.length === 0){
                        afterTests();
                    }
                });
            });
        }

    }

    function afterTests(){
        database.query('delete from users;', function(error, response){
            if (!error){
                testComplete();
            }
        });
    }
}








    // describe('test database connection', function(){
    //
    //     after(function(){
    //         testComplete();
    //     });
    //
    //     // it('get response from database', function(done){
    //     //     database.query('SELECT 1::int AS number', function(error, result){
    //     //
    //     //         if (!error){
    //     //             if (result[0].number === 1){
    //     //                 done();
    //     //             } else {
    //     //                 throw 'expected: 1\nreturned:'+result[0].number;
    //     //             }
    //     //         } else {
    //     //             throw error;
    //     //         }
    //     //
    //     //     });
    //     // });
    //
    //     describe('test results are returned from the database', function(){
    //
    //         // before(function(done){
    //         //
    //         //     database.query('delete from users;', function(error, response){
    //         //         if (!error){
    //         //
    //         //             var newUser = {
    //         //                 username: 'clark',
    //         //                 password: 'kent',
    //         //                 email: 'clark@glitr.io',
    //         //                 loginHash: 'dummyHash'
    //         //             }
    //         //             database.create('users', newUser, function(error, response){
    //         //                 if (!error){
    //         //                     done();
    //         //                 }
    //         //             });
    //         //         }
    //         //     });
    //         });
    //
    //         after(function(done){
    //             database.query('delete from users;', function(error, response){
    //                 if (!error){
    //                     done();
    //                 }
    //             });
    //         });
    //
    //         // it('should get results from the users table', function(done){
    //         //     database.query('select * from users', function(error, result){
    //         //         if (!error){
    //         //             if (!!result === true){
    //         //                 done();
    //         //             };
    //         //         } else {
    //         //             throw error;
    //         //         }
    //         //     });
    //         // });
    //
    //         // it('fails to get result from table that doesnt exist', function(done){
    //         //     database.query('select * from noSuchTable', function(error, result){
    //         //         if (error){
    //         //             done();
    //         //         }
    //         //     });
    //         // });
    //
    //         // it('finds clark.kent in the users table', function(done){
    //         //     database.query('select * from users where "username"=\'clark\'', function(error, result){
    //         //         if (!error){
    //         //             if (result[0].username === 'clark'){
    //         //                 done();
    //         //             }
    //         //         } else {
    //         //             throw error;
    //         //         }
    //         //     });
    //         // });
    //
    //         describe('CRUD using SQL commands', function(){
    //
    //             // it('create, read, update and delete a new record', function(done){
    //             //     database.query('insert into users (username, password, email, loginHash) values (\'john\', \'jones\', \'john@glitr.io\', \'dummyHash\');', function(error, result){
    //             //         if (!error){
    //             //             database.query('select * from users where "username"=\'john\'', function(error, result){
    //             //                 if (!error){
    //             //                     if (result[0].username === 'john'){
    //             //                             database.query('update users set email=\'martian.manhunter@test.com\' where username=\'john\'', function(error, result){
    //             //                                 if (!error){
    //             //                                     database.query('select * from users where "username"=\'john\'', function(error, result){
    //             //                                         if (!error){
    //             //                                             if (result[0].email === 'martian.manhunter@test.com'){
    //             //                                                 database.query('delete from users where username=\'john\'', function(error, result){
    //             //                                                     if (!error){
    //             //                                                         if (result.length === 0){
    //             //                                                             done();
    //             //                                                         }
    //             //                                                     } else {
    //             //                                                         throw error;
    //             //                                                     }
    //             //                                                 });
    //             //                                             }
    //             //                                         } else {
    //             //                                             throw error;
    //             //                                         }
    //             //                                     });
    //             //                                 } else {
    //             //                                     throw error;
    //             //                                 }
    //             //                             });
    //             //                     } else {
    //             //                         throw 'expected: \'john.jones\'\nreturned:'+result[0].username;
    //             //                     }
    //             //                 } else {
    //             //                     throw error;
    //             //                 }
    //             //             });
    //             //
    //             //         } else {
    //             //             throw error;
    //             //         }
    //             //     });
    //             // });
    //         });
    //     });
    //
    //     describe('test database CRUD interface', function(){
    //
    //         it('create record', function(done){
    //             database.create('users', {username: 'barry.allen', password:'barry.allen', email: 'barry.allen@test.com', loginHash: 'dummyHash'}, function(error, result){
    //                 if (!error){
    //                     if(!!result){
    //                         done();
    //                     }
    //                 } else {
    //                     throw 'failed to create record';
    //                 }
    //             });
    //         });
    //
    //         it('read record', function(done){
    //             database.read('users', ['*'], {username: 'barry.allen'}, function(error, result){
    //                 if (!error){
    //                     if (result[0].email === 'barry.allen@test.com'){
    //                         done();
    //                     }
    //                 } else {
    //                     throw 'failed to create record';
    //                 }
    //             });
    //         });
    //
    //         it('update record', function(done){
    //
    //             database.update('users', {username: 'barry.allen'}, {email: 'the.flash@test.com'}, function(error, result){
    //                 database.read('users', [], {username: 'barry.allen'}, function(error, result){
    //                     if (result[0].email === 'the.flash@test.com'){
    //                         done();
    //                     }
    //                 });
    //             });
    //         });
    //
    //         it('delete record', function(done){
    //
    //             database.delete('users', {username: 'barry.allen'}, function(error, result){
    //                 database.read('users', [], {username: 'barry.allen'}, function(error, result){
    //                     if (result.length === 0){
    //                         done();
    //                     }
    //                 });
    //             });
    //         });
    //     });
    // });

// }
