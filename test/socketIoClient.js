var socketClient = require('socket.io-client');

module.exports = function(){

    var socket;
    this.expectedMessages = [];
    this.loginHash = '';
    var connected = false;

    this.isConnected = function(){
        return connected;
    }

    this.connectToServer = function (username, password, loginHash, connectToServerCallback){

        socket = socketClient.connect('https://localhost:3001/chatEngine', {'force new connection': true});

        socket.on('connect', function () {

            if (!!loginHash){
                socket.emit('initialiseConnection', {username:username, loginHash:loginHash});
            } else if (!!password){
                socket.emit('initialiseConnection', {username:username, password:password});
            } else {
                connectToServerCallback('login details do not match required syntax', false);
            }

            setInterval(function checkForMessagesOnRemoteServer(){
                socket.emit('getPendingMessages', {loginHash: loginHash});
            }, 5000);

        });

        socket.on('loginResponse', function(loginResponse){
            if (loginResponse.code === 200){
                connected = true;
                loginHash = loginResponse.loginHash;
                connectToServerCallback(false, 'logged in user: '+username);
            } else {
                console.log('error logging into server');
            }
        });

        socket.on('recievingMessages', function(messages){
            console.log('recieving messages', messages);
            var expectedMessages = this.expectedMessages;
            for (var y = 0; y < messages.length; y++){
                for (var x = 0; x < expectedMessages.length; x++){
                    if (expectedMessages[x].identifier === messages[y].identifier){
                        delete expectedMessages[x];
                        expectedMessages.splice(x,1);
                        socket.emit('recievedMessage', {identifier: messages[y].identifier});
                        x--;
                    }
                }
            }

        }.bind(this));
    };

    this.disconectFromServer = function (disconnectedCallback){
        socket.disconnect();
        connected = false;
        disconnectedCallback();
    };

    this.emitMessageToUser = function(loginHash, message, emitMessageToUserCallback){
        socket.emit('emitMessageToUser', {message: message, loginHash: loginHash});

        socket.on('sentMessageResponse', function(sentMessageResponse){
            if (sentMessageResponse.code === 200){
                emitMessageToUserCallback(false, 'message sent \nfrom user: '+message.from+' \nto user: '+message.to);
            } else {
                emitMessageToUserCallback(sentMessageResponse);
            }
        });


    };
}
