var https = require('https');
var app = require('express')();
var fs = require('fs');

module.exports = function(server){

    var serverConfig = server.config;
    var chatController = new (require('./chatController'))(server);

    var httpsOptions = {
        key: fs.readFileSync(serverConfig.server.https_options.key),
        cert: fs.readFileSync(serverConfig.server.https_options.cert)
    };

    var httpsServer = https.createServer(httpsOptions, app).listen(serverConfig.server.port, function(){
      console.log('listening on port: '+serverConfig.server.port);
    });

    var io = require('socket.io')(httpsServer);
    var chatEngine = io.of("/chatEngine");
    var connectedClients = []

    setInterval(function(){
        console.log('connected users:',connectedClients.length);
    }, 1000);

    chatEngine.on('connection', function(socket){
        console.log('a user connected');

        socket.memeChat = {
            username: '',
            loginHash: '',
            pendingSentMessages: [],
        };

        setTimeout(function(){
            if (socket.memeChat.username === ''){
                socket.disconnect();
            }
        }, 20000);

        socket.on('disconnect', function () {
            console.log('disconnecting socket with username:', socket.memeChat.username);

            for (var x = 0; x < connectedClients.length; x++){
                if (connectedClients[x].memeChat.username === socket.memeChat.username){
                    delete connectedClients[x];
                    connectedClients.splice(x,1);
                    break;
                }
            }
        });








        socket.on('initialiseConnection', function(loginDetails){

            chatController.loginUser(loginDetails, loginResponse);

            function loginResponse(error, response){
                if (!error){
                    console.log('a user connected', loginDetails.username);
                    socket.emit('loginResponse', {code: 200, loginHash: response.loginHash});
                    socket.memeChat.username = loginDetails.username;
                    socket.memeChat.loginHash = response.loginHash;

                    for (var x = 0; x < connectedClients.length; x++){
                        if (connectedClients[x].memeChat.username === socket.memeChat.username){
                            socket.disconnect();
                            connectedClients.splice(x,1);
                            x--;
                        }
                    }

                    connectedClients.push(socket);
                    chatController.getPendingMessages(loginDetails.username, function(messages){
                        socket.emit('recievingMessages', messages);
                    });
                } else {
                    if (error === 'invalidLoginDetailsProvided'){
                        socket.emit('loginResponse', {code: 400});
                    }
                }
            }
        });













        socket.on('emitMessageToUser', function(messageDetails){
            chatController.validateUserAndLoginHash(messageDetails.message.from, messageDetails.loginHash, validateUserResponse);

            function validateUserResponse(error, response){
                if (!error){

                    chatController.recipientExistsAndNotBlocked(messageDetails.message.from, messageDetails.message.to, recipientExistsResponse);

                    function recipientExistsResponse(error, recipientExistsResponse){

                        if (!error){
                            var recipientIsValid = recipientExistsResponse.recipientIsValid;
                            var recipientIsOnline = recipientExistsResponse.recipientIsOnline;

                            if (recipientIsValid){
                                chatController.saveMessageToDatabase(messageDetails.message, saveMessageResponse);
                            } else {
                                socket.emit('sentMessageResponse', {code: 401, response: recipientExistsResponse} );
                            }

                            function saveMessageResponse(error, response){

                                if (!error){
                                    if (recipientIsOnline){
                                        socket.emit('sentMessageResponse', {code: 200});

                                        for (var x = 0; x < connectedClients.length; x++){
                                            if (connectedClients[x].memeChat.username === messageDetails.message.to){
                                                connectedClients[x].emit('recievingMessages', [messageDetails.message]);
                                                break;
                                            }
                                        }

                                    } else {
                                        socket.emit('sentMessageResponse', {code: 200});
                                    }
                                }
                            }

                        } else {
                            socket.emit('sentMessageResponse', {code: 401, response: error});
                        }
                    }

                    // socket.emit('sentMessageResponse', {code: 200, loginHash: response.loginHash});
                } else {
                    socket.emit('sentMessageResponse', {code: 401, response: 'sender is not valid'});
                }
            }
        });

        socket.on('recievedMessage', function(identifier){
            console.log('deleting message:', identifier);
            server.db.delete('messages', {identifier: identifier}, function(error, response){
                if (error){
                    console.log('failed to delete message:', identifier);
                }
            });
        });

        socket.on('getPendingMessages', function(recievedLoginHash){
            //check username and the recieved loginHash match
            //if user is valid, get all pending messages.

            var loginHash = socket.memeChat.loginHash;

            if (loginHash === recievedLoginHash){
                chatController.getPendingMessages(socket.memeChat.username, getPendingMessagesCallback);

                function getPendingMessagesCallback(error, messages){
                    if (error){
                        console.log(error);
                    } else {
                        socket.emit('recievingMessages', messages);
                    }
                }
            }
        });
    });

}
