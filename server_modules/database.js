var pg = require('pg');

module.exports = function(server){

    var config = server.config.database;

    var username = config.databaseUsername;
    var password = config.databasePassword;
    var host = config.host;
    var port = config.port;
    var database = config.databaseName;

    var conString = 'postgres://'+username+':'+password+'@'+host+':'+port+'/'+database+'';
    console.log(conString);

    this.query = function(query, queryCallback){

        pg.connect(conString, function(err, client, done) {
            if(err) {
                return console.error('error fetching client from pool', err);
                queryCallback('Error fetching client from pool', err, false);
            }

            client.query(query, function(err, result) {
                //call `done()` to release the client back to the pool
                done();

                if(err) {
                    queryCallback('Error running query:\n\nquery: '+query+'\n'+err, false);
                } else if (result.length === 0){
                    queryCallback('no results found for query: '+query, false);
                } else {
                    queryCallback(false, result.rows);
                }
            });
        });
    }

    this.create = function(table, newRecord, createCallback) {
        var fields = Object.keys(newRecord);

        var valuesString = '';
        for(var x = 0; x < fields.length; x++){

            var value = newRecord[fields[x]];
            if (typeof newRecord[fields[x]] === 'string'){
                value = '\''+newRecord[fields[x]]+'\'';
            }

            if (x != fields.length-1){
                value += ',';
            }

            valuesString += value;
        }

        var query = 'insert into '+table+' ('+fields.toString()+') values ('+valuesString+');';

        this.query(query, createCallback);
    }

    this.read = function(table, fields, criteria, readCallback){

        fields = (!!fields.length) ? fields:['*'];
        var query = 'select '+fields.toString()+' from '+table+' where';
        var criteriaFields = Object.keys(criteria);

        for (var x = 0; x < criteriaFields.length; x++){
            if (x>0){
                query += ' and';
            }
            query += ' '+criteriaFields[x]+'=\''+criteria[criteriaFields[x]]+'\'';
        }
        query += ';';

        this.query(query, readCallback);
    }

    this.update = function(table, criteria, update, updateCallback){

        var query = 'update '+table+' set';
        var updateFields = Object.keys(update);
        for (var x = 0; x < updateFields.length; x++){
            if (x>0){
                query += ',';
            }
            query += ' '+updateFields[x]+'=\''+update[updateFields[x]]+'\'';
        }

        query += ' where';
        var criteriaFields = Object.keys(criteria);
        for (var x = 0; x < criteriaFields.length; x++){
            if (x>0){
                query += ' and';
            }
            query += ' '+criteriaFields[x]+'=\''+criteria[criteriaFields[x]]+'\'';
        }
        query += ';';

        this.query(query, updateCallback);
    }

    this.delete = function (table, criteria, deleteCallback){
        var query = 'delete from '+table+' where';
        var criteriaFields = Object.keys(criteria);

        for (var x = 0; x < criteriaFields.length; x++){
            if (x>0){
                query += ' and';
            }
            query += ' '+criteriaFields[x]+'=\''+criteria[criteriaFields[x]]+'\'';
        }
        query += ';';

        this.query(query, deleteCallback);
    }
}
