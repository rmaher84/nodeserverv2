var crypto = require("crypto");

module.exports = function(server){

    this.loginUser = function(loginDetails, loginUserCallback){

        server.db.read('users', ['username', 'connected'], loginDetails, findUserInDatabase);

        function findUserInDatabase(error, response){

            if (!error){

                var randomHash = crypto.randomBytes(20).toString('hex');

                if (response.length === 1){
                    server.db.update('users', loginDetails, {connected: true, loginHash: randomHash}, updateUserResponse);
                } else {
                    loginUserCallback('invalidLoginDetailsProvided');
                }

                function updateUserResponse(error, response){
                    if (!error){
                        loginUserCallback(false, {loginHash: randomHash});
                    } else {
                        loginUserCallback('invalidLoginDetailsProvided');
                    }
                }

            } else {
                loginUserCallback('invalidLoginDetailsProvided')
            }
        }
    }

    this.recipientExistsAndNotBlocked = function (sender, recipient, recipientExistsAndNotBlockedCallback){
        server.db.read('users', ['username', 'connected', 'blockedusers'], {username: recipient}, function(error, response){
            if (!error && response.length === 1 && response[0].username === recipient){
                if (!response[0].blockedusers || response[0].blockedusers.indexOf(sender) < 0){
                    if (response[0].connected){
                        recipientExistsAndNotBlockedCallback(false, {recipientIsValid: true, recipientIsOnline: true});
                    } else {
                        recipientExistsAndNotBlockedCallback(false, {recipientIsValid: true, recipientIsOnline: false});
                    }
                } else {
                    recipientExistsAndNotBlockedCallback('user is blocked by recipient', false);
                }
            } else {
                recipientExistsAndNotBlockedCallback(error, 'user is not valid / does not exist');
            }
        });
    }

    this.validateUserAndLoginHash = function (username, loginHash, validateLoginHashCallback){
        server.db.read('users', ['username', 'loginhash'], {username: username, loginhash: loginHash}, function(error, response){
            if (!error && response.length === 1 && response[0].username === username){
                validateLoginHashCallback(false, true);
            } else {
                validateLoginHashCallback(error);
            }
        });
    }

    this.saveMessageToDatabase = function (message, saveMessageToDatabaseCallback){
        server.db.create('messages', {identifier: message.identifier, sender: message.from, recipient: message.to, message: message.message, }, function(error, response){
            if (!error){
                saveMessageToDatabaseCallback(false, {code: 200});
            } else {
                saveMessageToDatabaseCallback(error, {code: 401});
            }
        });
    }



    this.getPendingMessages = function (username, getPendingMessagesCallback){
        server.db.read('messages', ['identifier', 'sender', 'recipient', 'message', 'date'], {recipient: username}, function(error, messages){
            if (!error){
                getPendingMessagesCallback({code: 200, messages: messages});
            } else {
                getPendingMessagesCallback({code: 401, messages: []});
            }
        });
    }
}
