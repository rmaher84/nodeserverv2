
//  NODEJS MODULES -----------------------------
var fs = require('fs');
//  NODEJS MODULES -----------------------------

//  CUSTOM MODULES -----------------------------
var config = require('./config.json');
var database = require('./server_modules/database');
var instantMessageEngine = require('./server_modules/instantMessageEngine');
//  CUSTOM MODULES -----------------------------

//  EXPOSE CORE MODULES -----------------------------
exports.config = config;
// exports.db = db;
//  EXPOSE CORE MODULES -----------------------------

//  INITIALISE CORE MODULES -----------------------------
exports.db = new database(this);
var io = new instantMessageEngine(this);
//  INITIALISE CORE MODULES -----------------------------
